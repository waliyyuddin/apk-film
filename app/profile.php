<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $fillable = ['username', 'alamat', 'bio'];
    // protected $guarded = [''];

    public function user() {
        return $this->hasOne('App\User');
    }
}
