<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cast extends Model
{
    protected $fillable = ['nama', 'bio', 'usia'];
    // protected $guarded = [''];
}
