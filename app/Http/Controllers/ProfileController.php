<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\profile;
use App\User;
use DB;
use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $join = DB::table('profiles') 
            ->join('users', 'profiles.users_id', '=', 'users.id') 
            ->select('profiles.*', 'users.email', 'users.level') 
            ->get();
        $user = User::all();
        return view('profile.index', compact('join','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $join = DB::table('profiles') 
            ->join('users', 'profiles.users_id', '=', 'users.id') 
            ->select('profiles.*', 'users.email', 'users.level') 
            ->get();
        $user = User::all();
        return view('profile.index', compact('join','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = profile::findorfail($id);
        $user = User::all();
        return view('profile.edit', compact('profile', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'photo' => 'mimes:jpeg,jpg,png|max:3200',
            'username' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
            'users_id' => 'required'
        ]);

        $profile = Profile::findorfail($id);

        if ($request->has('photo')){
            $path = "upload/product/";
            File::delete($path . $profile->photo);
            $photo = $request->photo;
            $new_photo = time() . ' - ' . $photo->getClientOriginalName();
            $photo->move('upload/profile/', $new_photo);
            $post_data = [
                "photo" => $new_photo,
                "username" => $request->username,
                "alamat" => $request->alamat,
                "bio" => $request->bio,
                "users_id" => $request->users_id
            ];
        }else{
            $post_data = [
                "username" => $request->username,
                "alamat" => $request->alamat,
                "bio" => $request->bio,
                "users_id" => $request->users_id
            ];
        }

        $profile->update($post_data);
        return redirect('/profile');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = profile::findorfail($id);
        $profile->delete();

        $path = "upload/profile/";
        File::delete($path . $profile->photo);

        return redirect()->route('profile.index'); 
    }
}
