<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\film;
use DB;
use File;
use App\cast;
use App\genre;
use App\peran;
use Auth;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $join = DB::table('films')
            ->join('casts', 'films.nama', '=', 'casts.nama')
            ->join('perans', 'films.peran', '=', 'perans.peran')
            ->join('genres', 'films.genres_id', '=', 'genres.genre')
            ->select('films.*', 'casts.*', 'perans.*', 'genres.*')
            ->get();

        $film = film::all();
        return view('film.index', compact('film', 'join'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = genre::all();
        $peran = peran::all();
        $cast = cast::all();
        return view('film.create', compact('genre', 'peran', 'cast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cover' => 'required|mimes:jpeg,jpg,png|max:2200',
            'judul' => 'required',
            'deskripsi' => 'required',
            'nama' => 'required',
            'peran' => 'required',
            'genres_id' => 'required'
        ]);

        $cover = $request->cover;
        $new_cover = time() . ' - ' . $cover->getClientOriginalName();

        $film = film::create([
            "cover" => $new_cover,
            "judul" => $request->judul,
            "deskripsi" => $request->deskripsi,
            "nama" => $request->nama,
            "peran" => $request->peran,
            "genres_id" => $request->genres_id
        ]);

        $cover->move('upload/film/', $new_cover);
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = film::findorfail($id);
        return view('film.show', compact('film'));  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = film::findorfail($id);
        $peran = peran::all();
        $cast = cast::all();
        $genre = genre::all();

        return view('film.edit', compact('film', 'peran', 'cast', 'genre'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cover' => 'mimes:jpeg,jpg,png|max:2200',
            'judul' => 'required',
            'deskripsi' => 'required',
            'nama' => 'required',
            'peran' => 'required',
            'genres_id' => 'required'
        ]);

        $film = film::findorfail($id);

        if ($request->has('cover')){
            $path = "upload/film/";
            File::delete($path . $film->cover);
            $cover = $request->cover;
            $new_cover = time() . ' - ' . $cover->getClientOriginalName();
            $cover->move('upload/film/', $new_cover);
            $post_data = [
                "cover" => $new_cover,
                "judul" => $request->judul,
                "deskripsi" => $request->deskripsi,
                "nama" => $request->nama,
                "peran" => $request->peran,
                "genres_id" => $request->genres_id
            ];
        }else{
            $post_data = [
                "judul" => $request->judul,
                "deskripsi" => $request->deskripsi,
                "nama" => $request->nama,
                "peran" => $request->peran,
                "genres_id" => $request->genres_id
            ];
        }

        $film->update($post_data);
        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = film::findorfail($id);
        $film->delete();

        $path = "upload/film/";
        File::delete($path . $film->cover);

        return redirect()->route('film.index'); 
    }
}
