<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\peran;
use App\cast;
use App\film;
use DB;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $join = DB::table('perans')
            ->join('films', 'perans.films_id', '=', 'films.id')
            ->join('casts', 'perans.casts_id', '=', 'casts.nama')
            ->select('perans.*', 'casts.*', 'films.*')
            ->get();

        $peran = peran::all();
        return view('peran.index', compact('peran', 'join'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cast = cast::all();
        $film = film::all();
        return view('peran.create', compact('cast', 'film'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'peran' => 'required',
            'films_id' => 'required',
            'casts_id' => 'required'
        ]);

        $peran = peran::create([
            "nama" => $request->nama,
            "peran" => $request->peran,
            "films_id" => $request->films_id,
            "casts_id" => $request->casts_id
        ]);

        return redirect('/peran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peran = peran::findorfail($id);
        $film = film::all();
        $cast = cast::all();
        return view('peran.edit', compact('peran', 'film', 'cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'peran' => 'required',
            'films_id' => 'required',
            'casts_id' => 'required'
        ]);

        $peran = peran::findorfail($id);
        $peran->update([
            "nama" => $request->nama,
            "peran" => $request->peran,
            "films_id" => $request->films_id,
            "casts_id" => $request->casts_id
        ]);

        return redirect('/peran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        peran::destroy($id);
        return redirect('/peran');
    }
}
