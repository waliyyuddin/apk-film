<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    protected $fillable = ['judul', 'deskripsi', 'nama', 'peran', 'genre', 'cover','genres_id'];
    // protected $guarded = [''];
}
