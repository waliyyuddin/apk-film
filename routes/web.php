<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'TamuController@index');

// Route::get('/e', function () {
//     return view('anime.rincian');
// });

Route::get('/d', function () {
    return view('anime.rincian');
});

Route::get('/beranda', function () {
    return view('admin.beranda');
});

Route::get('/list', function () {
    return view('admin.index');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/login', function () {
    return view('register');
});

Route::get('/hai', function () {
    return view('anime.details');
});

Route::get('/genres', function () {
    return view('anime.genre');
});

Route::get('/master1', function() {
    return view('anime.master');
});

Route::get('/master2', function() {
    return view('admin.master');
});

Route::resource('tamu', 'TamuController');

Route::group(['middleware' => ['auth', 'CekLevel:admin']], function() {
    Route::resource('genre', 'GenreController');
});

Route::group(['middleware' => ['auth']], function() {
    Route::get('/tamu/{id}', 'TamuController@show');
    
    
    Route::resource('view', 'ViewController');
    // Route::get('/view/{$id}', 'ViewController@shows');
    
    // route profile
    Route::resource('profile', 'ProfileController');
    
    // route genre
    
    // reoute cast
    Route::resource('cast', 'CastController');
    
    // route film
    Route::resource('film', 'FilmController');
    
    // route peran
    Route::resource('peran', 'PeranController');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
