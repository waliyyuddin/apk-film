@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Edit Film</h2>
    <div class="card-header">
        <a href="/film" class="btn btn-primary">Kembali</a>
    </div>
    <div class="row">
        <div class="col-3">
            <img src="{{asset('upload/film/'.$film->cover)}}" width="100%" height="" alt="">
        </div>
        <div class="col-9">
            <table class="table" border="1" cellpadding="1">
                <tr>
                    <td>
                        <h4>Judul</h4>
                    </td>
                    <td>
                        <p>{{$film->judul}}</p>
                    </td>
                </tr>
                <tr>                    
                    <td>
                        <h4>Deskripsi</h4>
                    </td>                   
                    <td>
                        <p>{{$film->deskripsi}}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>Nama Pemain</h4>
                    </td>
                    <td>
                        <p>{{$film->nama}}</p>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <h4>Peran</h4>
                    </td>
                    <td>
                        <ul>
                            <p>{{$film->peran}}</p>
                        </ul>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <h4>Genre</h4>
                    </td>
                    <td>
                        <p>{{$film->genres_id}}</p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection