@extends('admin.master')

@section('content')
<div class="card-header">
    <a href="/film/create" class="btn btn-primary">Tambah Data</a>
</div>
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Data Film</h2>
    <table class="data-table table nowrap">
        <thead>
            <tr>
                <th>No</th>
                <th>cover</th>
                <th>Judul</th>
                <th>deskripsi</th>
                <th>nama</th>
                <th>Peran</th>
                <th>genre</th>
                <th class="datatable-nosort">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($film as $key=>$value)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td class="table-plus">
                        <img src="{{asset('upload/film/'.$value->cover)}}" width="70" height="70" alt="">
                    </td>
                    <td>
                        <h5 class="font-16">{{ $value->judul }}</h5>
                    </td>
                    <td>{{ $value->deskripsi }}</td>
                    <td>{{ $value->nama }}</td>
                    <td>{{ $value->peran }}</td>
                    <td>{{ $value->genres_id }}</td>
                    <td>
                        <div class="dropdown">
                            <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                <i class="dw dw-more"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                <a class="dropdown-item" href="/film/{{ $value->id }}/edit"><i class="dw dw-edit2"></i> Edit</a>
                                <a class="dropdown-item" href="/film/{{ $value->id }}"><i class="dw dw-edit2"></i> Show</a>
                                <form action="/film/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                    <button class="dropdown-item"><i class="dw dw-delete-3"></i> Delete</button>
                                </form>
                            </div>
                        </div>
                    </td>                
                </tr>
            @empty
                <tr style="text-align: center">
                    <td colspan="8">No data Available</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection