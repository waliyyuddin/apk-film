@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Tambah Film</h2>
    <div class="card-header">
        <a href="/film" class="btn btn-primary">Kembali</a>
    </div>
    <form action="/film" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-box mb-30">
            <div class="mb-3">
                <label for="formFile" class="form-label h4 pd-20">Cover</label>
                <input class="form-control" name="cover" type="file" id="formFile">
                </div>
            @error('cover')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="judul" class="h4 pd-20">Judul</label><br>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
            @error('judul')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="deskripsi" class="h4 pd-20">Deskripsi</label><br>
            <textarea name="deskripsi" id="deskripsi" cols="107" rows="10"></textarea>
            @error('deskripsi')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="nama" class="h4 pd-20">Nama Pemain</label>
            <select class="custom-select" name="nama" id="nama">
                <option value="">--Silahkan Pilih Nama Pemain--</option>
                @foreach ($cast as $item)
                    <option value="{{$item->nama}}">{{$item->nama}}</option>
                @endforeach
            </select>
            @error('nama')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="peran" class="h4 pd-20">Peran</label>
            <input type="text" name="peran" id="peran" class="form-control" placeholder="Masukan Peran">
            @error('peran')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="genres_id" class="h4 pd-20">Genre</label>
            <select class="custom-select" name="genres_id" id="genres_id">
                <option value="">--Silahkan Pilih Genre--</option>
                @foreach ($genre as $item)
                    <option value="{{$item->id}}">{{$item->genre}}</option>
                @endforeach
            </select>
            @error('genres_id')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
        <button type="reset" class="btn btn-warning">Reset</button>
    </form>
</div>
@endsection