@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Edit Film</h2>
    <div class="card-header">
        <a href="/film" class="btn btn-primary">Kembali</a>
    </div>
    <form action="/film/{{ $film->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="card-box mb-30">
            <div class="form-group">
                <label for="cover">Cover</label>
                <input type="file" class="form-control" name="cover" value="{{ $film->cover }}" id="cover">
                @error('cover')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="card-box mb-30">
            <label for="judul">Judul</label><br>
            <input type="text" class="form-control" name="judul" id="judul" value="{{ $film->judul }}">
            @error('judul')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="deskripsi">Deskripsi</label><br>
            <textarea name="deskripsi" id="deskripsi" cols="107" rows="10">{{ $film->deskripsi }}</textarea>
            @error('deskripsi')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="nama">Nama Pemain</label><br>
            <select class="custom-select" name="nama" id="nama">
                <option value="">--Silahkan Pilih Nama Pemain--</option>
                @foreach ($cast as $item)
                    @if ($item->nama === $film->nama)
                        <option value="{{ $item->nama }}" selected>{{ $item->nama }}</option>
                    @else
                        <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                    @endif
                @endforeach
            </select>
            @error('nama')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="peran" class="h4 pd-20">Peran</label>
            <input type="text" name="peran" id="peran" class="form-control" value="{{ $film->peran }}">
            @error('peran')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="genres_id" class="h4 pd-20">Genre</label>
            <select class="custom-select" name="genres_id" id="genres_id">
                <option value="">--Silahkan Pilih Genre--</option>
                @foreach ($genre as $item)
                    @if ($item->id === $film->id)
                        <option value="{{ $item->id }}" selected>{{ $item->genre }}</option>
                    @else
                        <option value="{{ $item->id }}">{{ $item->genre }}</option>
                    @endif
                @endforeach
            </select>
            @error('genres_id')
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="reset" class="btn btn-warning">Reset</button>
    </form>
</div>
@endsection