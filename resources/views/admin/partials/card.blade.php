@yield('title')
<div class="pd-ltr-20">
    <div class="card-box pd-20 height-100-p mb-30">
        @yield('content')
    </div>
</div>
<div class="footer-wrap pd-20 mb-20 card-box">
    DeskApp - Bootstrap 4 Admin Template By <a href="https://github.com/dropways" target="_blank">Ankit Hingarajiya</a>, Apk by : Waliyyuddin. Instagram : <a href="https://www.instagram.com/waliyyuddin_wali/">Waliyyuddin</a>
</div>