<div class="left-side-bar">
    <div class="brand-logo">
        <a href="index.html">
            <img src="{{ asset('/admin/deskapp2-master/vendors/images/deskapp-logo.svg') }}" alt="" class="dark-logo">
            <img src="{{ asset('/admin/deskapp2-master/vendors/images/deskapp-logo-white.svg') }}" alt="" class="light-logo">
        </a>
        <div class="close-sidebar" data-toggle="left-sidebar-close">
            <i class="ion-close-round"></i>
        </div>
    </div>
    <div class="menu-block customscroll">
        <div class="sidebar-menu">
            <ul id="accordion-menu">
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="micon dw dw-house-1"></span><span class="mtext">Home</span>
                    </a>
                    <ul class="submenu">
                        <li><a href="/beranda">Beranda</a></li>
                        <li><a href="/list">Dashboard</a></li>
                    </ul>
                </li>
                <li>
                    <a href="/film" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-browser2"></span><span class="mtext">Film</span> 
                    </a>
                </li>
                <li>
                    <a href="/profile" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-invoice"></span><span class="mtext">Profile</span>
                    </a>
                </li>
                <li>
                    <a href="/cast" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-library"></span><span class="mtext">Cast</span>
                    </a>
                </li>
                <li>
                    <a href="/peran" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-apartment"></span><span class="mtext"> Peran </span>
                    </a>
                </li>
                <li>
                    <a href="/genre" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-list3"></span><span class="mtext">Genre</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="mobile-menu-overlay"></div>