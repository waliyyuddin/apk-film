@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman List</h2>
    <table class="data-table table nowrap">
        <thead>
            <tr>
                <th>#</th>
                <th>Cast</th>
                <th>Peran</th>
                <th>Film</th>
                <th>Genre</th>
            </tr>
        </thead>
        <tbody>
                <tr>
                    <td> 1 </td>
                    <td>- Lee Sung Kyung <br>
                        - Nam Joo hyuk
                    </td>
                    <td>- Kim Bok Joo <br>
                        - Joon Jung Hyung
                    </td>
                    <td>Weightlifthing Fairy Kim Bok Joo</td>
                    <td>Romantic</td>     
                </tr>
                <tr>
                    <td> 2 </td>
                    <td>Tom Holland</td>
                    <td>Peter Parker</td>
                    <td>Spider-Man Homecoming</td>
                    <td>Action</td>     
                </tr>
                <tr>
                    <td> 3 </td>
                    <td>Tom Holland</td>
                    <td>Peter Parker</td>
                    <td>Spider-Man Far From Home</td>
                    <td>Action</td>     
                </tr>
                <tr>
                    <td> 4 </td>
                    <td>Benedict Cumberbatch</td>
                    <td>Doctor Strange</td>
                    <td>Doctor Strange</td>
                    <td>Action</td>     
                </tr>
                <tr>
                    <td> 5 </td>
                    <td>Herjunot Ali</td>
                    <td>Zafran</td>
                    <td>5 cm</td>
                    <td>Adventure</td>     
                </tr>
        </tbody>        
    </table>
</div>
@endsection