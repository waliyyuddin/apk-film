@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Profile</h2>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="profile" class="table table-bordered table-striped" style="text-align: center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Photo</th>
                                <th>Username</th>
                                <th>Alamat</th>
                                <th>Biodata</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($join as $key=>$value)
                            <tr>
                                <td>{{ $key + 1 }}</th>
                                <td>{{$value->photo}}</td>
                                <td>{{$value->username}}</td>
                                <td>{{$value->alamat}}</td>
                                <td>{{$value->bio}}</td>
                                <td><a href="/profile/{{ $value->id }}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a></td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="dw dw-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                            <a class="dropdown-item" href="/cast/{{ $value->id }}/edit"><i class="dw dw-edit2"></i> Edit</a>
                                            <form action="/cast/{{ $value->id }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                                <button class="dropdown-item"><i class="dw dw-delete-3"></i> Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty
                                <tr style="text-align: center">
                                    <td colspan="8">No data Available</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
    <script>
        $(function() {
            $("#profile").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "print", "colvis"]
            }).buttons().container().appendTo('#profile_wrapper .col-md-6:eq(0)');
            $("#genre").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "print", "colvis"]
            }).buttons().container().appendTo('#genre_wrapper .col-md-6:eq(0)');
        });
    </script>
@endpush