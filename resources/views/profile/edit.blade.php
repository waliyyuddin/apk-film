@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Edit Profile</h2>
    <div class="card-header">
        <a href="/profile" class="btn btn-primary">Kembali</a>
    </div>
    <form action="/profile/{{ $profile->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="card-box mb-30">
            <div class="form-group">
                <label for="photo">Photo</label>
                <input type="file" class="form-control" name="photo" value="{{ $profile->photo }}" id="photo">
                @error('photo')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="card-box mb-30">
            <label for="username">Username</label><br>
            <input type="text" class="form-control" name="username" id="username" value="{{ $film->username }}">
            @error('username')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="alamat">Alamat</label><br>
            <input type="text" class="form-control" name="alamat" id="alamat" value="{{ $film->alamat }}">
            @error('alamat')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="bio">Bio</label><br>
            <input type="text" class="form-control" name="bio" id="bio" value="{{ $film->bio }}">
            @error('bio')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="reset" class="btn btn-warning">Reset</button>
    </form>
</div>
@endsection
@push('script')
    <script>
        $(function() {
            $("#profile").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "print", "colvis"]
            }).buttons().container().appendTo('#profile_wrapper .col-md-6:eq(0)');
            $("#genre").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "print", "colvis"]
            }).buttons().container().appendTo('#genre_wrapper .col-md-6:eq(0)');
        });
    </script>
@endpush