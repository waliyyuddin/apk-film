@extends('admin.master')

@section('content')
<div class="card-header">
    <a href="/cast/create" class="btn btn-primary">Tambah Data</a>
</div>
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Cast</h2>
    <table class="data-table table nowrap">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Bio</th>
                <th>Usia</th>
                <th class="datatable-nosort">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$value)
            <tr>
                <td> {{ $key + 1 }} </td>
                <td>
                    <h5 class="font-16">{{ $value->nama }}</h5>
                </td>
                <td>{{ $value->bio }}</td>
                <td>{{ $value->usia }}</td>
                <td>
                    <div class="dropdown">
                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                            <i class="dw dw-more"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                            <a class="dropdown-item" href="/cast/{{ $value->id }}/edit"><i class="dw dw-edit2"></i> Edit</a>
                            <form action="/cast/{{ $value->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                                <button class="dropdown-item"><i class="dw dw-delete-3"></i> Delete</button>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
            @empty
                <tr style="text-align: center">
                    <td colspan="6">No data Available</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection