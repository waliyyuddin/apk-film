@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Tambah Cast</h2>
    <div class="card-header">
        <a href="/cast" class="btn btn-primary">Kembali</a>
    </div>
    <form action="/cast" method="POST">
        @csrf
        <div class="card-box mb-30">
            <label for="nama" class="h4 pd-20">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="bio" class="h4 pd-20">Bio</label></br>
            <textarea name="bio" id="" cols="107" rows="10"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="usia" class="h4 pd-20">Usia</label>
            <input type="number" name="usia" id="usia" class="form-control" placeholder="Masukan Usia">
            @error('usia')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
        <button type="reset" class="btn btn-warning">Reset</button>
    </form>
</div>
@endsection