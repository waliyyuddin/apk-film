@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Edit Genre</h2>
    <div class="card-header">
        <a href="/genre" class="btn btn-primary">Kembali</a>
    </div>
    <form action="/genre/{{$genre->id}}" method="post">
        @csrf
        @method('put')
        <div class="card-box mb-30">
            <label for="genre" class="h4 pd-20">Genre</label>
            <input type="text" name="genre" id="genre" class="form-control" value="{{ $genre->genre }}">
            @error('genre')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="reset" class="btn btn-warning">Reset</button>
    </form>
@endsection