@extends('anime.master')

@section('content')
<section class="hero">
    <div class="container">
        <div class="hero__slider owl-carousel">
            <div class="hero__items set-bg" data-setbg="{{ asset('/img/dr.jpg') }}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <div class="label">Action</div>
                            <h2>Doctor Strange</h2>
                            <p>Doctor Strange is a 2016 American superhero film based...</p>
                            @auth
                            <a href="/genres"><span>Watch Now</span> <i class="fa fa-angle-right"></i></a>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero__items set-bg" data-setbg="{{ asset('/img/jumspder.jpg') }}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <div class="label">Action</div>
                            <h2>Spider-Man Homecoming</h2>
                            <p>Spider-Man: Homecoming ist ein US-amerikanischer...</p>
                            @auth
                            <a href="/genres"><span>Watch Now</span> <i class="fa fa-angle-right"></i></a>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero__items set-bg" data-setbg="{{ asset('/img/bokjoo.jpg') }}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <div class="label">Romantic</div>
                            <h2>weightlifting fairy kim bok joo</h2>
                            <p>Weightlifting Fairy Kim Bok-joo (Korean: 역도요정 김복주;...</p>
                            @auth
                            <a href="/genres"><span>Watch Now</span> <i class="fa fa-angle-right"></i></a>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="product spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="trending__product">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <div class="section-title">
                                <h4>Trending Now</h4>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="btn__all">
                                <a href="#" class="primary-btn">View All <span class="arrow_right"></span></a>
                            </div>
                        </div>
                    </div>
                    @auth
                    <div class="row">
                        @forelse ($film as $key=>$value)
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="#')">
                                    <div class="product__item__pic set-bg">
                                        <img src="{{asset('upload/film/'.$value->cover)}}" alt="">
                                        <div class="ep">18 / 18</div>
                                        <div class="comment"><i class="fa fa-comments"></i> 11</div>
                                        <div class="view"><i class="fa fa-eye"></i> 9141</div>
                                    </div>
                                </div>
                                <div class="product__item__text">
                                    <ul>
                                        <li>Active</li>
                                        <li>{{ $value->genres_id }}</li>
                                    </ul>
                                    <h5><a href="/tamu/{{ $value->id }}">{{ $value->judul }}</a></h5>
                                </div>
                            </div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                    @endauth
                    @guest
                    <div class="row">
                        @forelse ($film as $key=>$value)
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="#')">
                                    <div class="product__item__pic set-bg">
                                        <img src="{{asset('upload/film/'.$value->cover)}}" alt="">
                                        <div class="ep">18 / 18</div>
                                        <div class="comment"><i class="fa fa-comments"></i> 11</div>
                                        <div class="view"><i class="fa fa-eye"></i> 9141</div>
                                    </div>
                                </div>
                                <div class="product__item__text">
                                    <ul>
                                        <li>Active</li>
                                        <li>{{ $value->genres_id }}</li>
                                    </ul>
                                    <h5><a href="/tamu/{{ $value->id }}">{{ $value->judul }}</a></h5>
                                </div>
                            </div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                    @endguest
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-8">
                <div class="product__sidebar">
                    <div class="product__sidebar__view">
                        <div class="section-title">
                            <h5>Top Views</h5>
                        </div>
                        <ul class="filter__controls">
                            <li class="active" data-filter="*">All</li>
                            <li data-filter=".a">Action</li>
                            <li data-filter=".e">Romantic</li>
                            <li data-filter=".i">Drama</li>
                            <li data-filter=".u">Horror</li>
                        </ul>
                        <div class="filter__gallery">
                            <div class="product__sidebar__view__item set-bg mix a"
                                data-setbg="img/sidebar/tv-1.jpg">
                                <div class="product__item__pic set-bg">
                                    <img src="{{asset('upload/film/1631185925 - 250px-Weightlifting_Fairy_Kim_Bok_Joo_Poster.jpg')}}" alt="film">
                                </div>
                                <div class="ep">18 / ?</div>
                                <div class="view"><i class="fa fa-eye"></i> 9141</div>
                                <h5><a href="#">weightlifting fairy kim bok joo</a></h5>
                            </div>                       
                            <div class="product__sidebar__view__item set-bg mix e"
                                data-setbg="img/sidebar/tv-1.jpg">
                                <div class="product__item__pic set-bg">
                                    <img src="{{asset('upload/film/1631265762 - OIP.png')}}" alt="film">
                                </div>
                                <div class="ep">18 / ?</div>
                                <div class="view"><i class="fa fa-eye"></i> 9141</div>
                                <h5><a href="#">The Bride of Habaek</a></h5>
                            </div>                       
                            <div class="product__sidebar__view__item set-bg mix i"
                                data-setbg="img/sidebar/tv-1.jpg">
                                <div class="product__item__pic set-bg">
                                    <img src="{{asset('upload/film/1631265857 - spidey.png')}}" alt="film">
                                </div>
                                <div class="ep">18 / ?</div>
                                <div class="view"><i class="fa fa-eye"></i> 9141</div>
                                <h5><a href="#">Spider-Man Far From Home</a></h5>
                            </div>                       
                            <div class="product__sidebar__view__item set-bg mix u"
                                data-setbg="img/sidebar/tv-1.jpg">
                                <div class="product__item__pic set-bg">
                                    <img src="{{asset('upload/film/1631265952 - Dr.str.jpg')}}" alt="film">
                                </div>
                                <div class="ep">18 / ?</div>
                                <div class="view"><i class="fa fa-eye"></i> 9141</div>
                                <h5><a href="#">Doctor Strange</a></h5>
                            </div>                       
                            <div class="product__sidebar__view__item set-bg mix action"
                                data-setbg="img/sidebar/tv-1.jpg">
                                <div class="product__item__pic set-bg">
                                    <img src="{{asset('upload/film/1631270538 - spider.jpg')}}" alt="film">
                                </div>
                                <div class="ep">18 / ?</div>
                                <div class="view"><i class="fa fa-eye"></i> 9141</div>
                                <h5><a href="#">Spider_Man Homecoming</a></h5>
                            </div>                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection