<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Film | Template</title>

    <!-- Google Font -->
<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/elegant-icons.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/plyr.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/nice-select.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/slicknav.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/style.css') }}" type="text/css">
    @stack('style')
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('anime.partials.navbar')
    <!-- Header End -->

    <!-- Hero Section Begin -->
    @yield('jumbotron')
    <!-- Hero Section End -->

    <!-- Product Section Begin -->
    @yield('content')
    <!-- Product Section End -->
</body>

<!-- Footer Section Begin -->
<footer class="footer">
    <div class="page-up">
        <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="footer__logo">
                    <a href="{{ asset('/anime/anime-main/index.html') }}"><img src="{{ asset('/anime/anime-mainimg/logo.png') }}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="footer__nav">
                    <ul>
                        <li class="active"><a href="{{ asset('/anime/anime-main/index.html') }}">Homepage</a></li>
                        <li><a href="{{ asset('/anime/anime-main/categories.html') }}">Categories</a></li>
                        <li><a href="{{ asset('/anime/anime-main/blog.html') }}">Our Blog</a></li>
                        <li><a href="#">Contacts</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                <p>Apk by : Waliyyuddin. Instagram : <a href="https://www.instagram.com/waliyyuddin_wali/">Waliyyuddin</a></p>

              </div>
          </div>
      </div>
  </footer>
  <!-- Footer Section End -->

  <!-- Search model Begin -->
  <div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch"><i class="icon_close"></i></div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
</div>
<!-- Search model end -->

<!-- Js Plugins -->
<script src="{{ asset('/anime/anime-main/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('/anime/anime-main/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/anime/anime-main/js/player.js') }}"></script>
<script src="{{ asset('/anime/anime-main/js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('/anime/anime-main/js/mixitup.min.js') }}"></script>
<script src="{{ asset('/anime/anime-main/js/jquery.slicknav.js') }}"></script>
<script src="{{ asset('/anime/anime-main/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('/anime/anime-main/js/main.js') }}"></script>
@stack('scripts')

</html>