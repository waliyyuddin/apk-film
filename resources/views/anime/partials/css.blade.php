@push('css')
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/elegant-icons.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/plyr.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/nice-select.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/slicknav.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/anime/anime-main/css/style.css') }}" type="text/css">
@endpush