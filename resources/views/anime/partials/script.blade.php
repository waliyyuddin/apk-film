@push('scripts')
    <script src="{{ asset('/anime/anime-main/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('/anime/anime-main/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/anime/anime-main/js/player.js') }}"></script>
    <script src="{{ asset('/anime/anime-main/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('/anime/anime-main/js/mixitup.min.js') }}"></script>
    <script src="{{ asset('/anime/anime-main/js/jquery.slicknav.js') }}"></script>
    <script src="{{ asset('/anime/anime-main/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/anime/anime-main/js/main.js') }}"></script>
@endpush