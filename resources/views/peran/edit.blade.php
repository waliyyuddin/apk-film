@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Edit Peran</h2>
    <div class="card-header">
        <a href="/peran" class="btn btn-primary">Kembali</a>
    </div>
    <form action="/peran/{{$peran->id}}" method="post">
        @csrf
        @method('put')
        <div class="card-box mb-30">
            <label for="nama" class="h4 pd-20">Nama</label>
            <select class="custom-select" name="nama" id="nama">
                <option value=" {{ $peran->nama }} ">--Silahkan Pilih Nama Pemain--</option>
                @foreach ($cast as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="peran" class="h4 pd-20">Peran</label>
            <textarea name="peran" id="" cols="107" rows="10">{{ $peran->peran }}</textarea>
            @error('peran')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="reset" class="btn btn-warning">Reset</button>
    </form>
@endsection