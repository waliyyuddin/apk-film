@extends('admin.master')

@section('content')
<div class="card-box mb-30">
    <h2 class="h4 pd-20">Halaman Tambah Peran</h2>
    <div class="card-header">
        <a href="/peran" class="btn btn-primary">Kembali</a>
    </div>
    <form action="/peran" method="POST">
        @csrf
        <div class="card-box mb-30">
            <label for="casts_id" class="h4 pd-20">Nama</label>
            <select class="custom-select" name="casts_id" id="casts_id">
                <option value="">--Silahkan Pilih Nama Pemain--</option>
                @foreach ($cast as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
            @error('casts_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="card-box mb-30">
            <label for="films_id" class="h4 pd-20">Peran</label></br>
            <input type="text" name="films_id" id="films_id" class="form-control" placeholder="Masukan Peran">
            @error('films_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
        <button type="reset" class="btn btn-warning">Reset</button>
    </form>
</div>
@endsection